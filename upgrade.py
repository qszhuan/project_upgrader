# -*- coding: utf-8 -*-

import glob
import os
import pprint
import re

from processor import ProjectFileProcessor, WebConfigProcessor, AppConfigProcessor, PackageConfigProcessor

slnFolder = '../csn.bikes/'


class SolutionExplore(object):
    def __init__(self, sln_folder):
        self.slnFolder = sln_folder

    def get_projects(self):
        os.chdir(self.slnFolder)
        for file in glob.glob("*.sln"):
            return list(self._extract_projects(file))

    def _extract_projects(self, file):
        with open(file) as solution_file:
            lines = solution_file.readlines()
            for line in lines:
                matches = re.search('([^"]+?)(?<=.csproj)', line)
                if matches:
                    project_path = matches.group(1)
                    project_path = os.path.join(self.slnFolder, project_path)
                    project_folder = os.path.dirname(project_path)
                    result = {'folder': project_folder, 'project': project_path}

                    for each in ['app.config', 'packages.config', 'web.config', 'web.Staging.config',
                                 'web.Release.config', 'web.Production.config']:
                        path = os.path.join(project_folder, each)
                        if os.path.exists(path):
                            result[each] = path
                    yield result


class ProjectUpgrader(object):
    def __init__(self, project_dict):
        self.proj_dict = project_dict
        self.processor_mapping = {
            'project': ProjectFileProcessor,
            'web.config': WebConfigProcessor,
            'web.Release.config': WebConfigProcessor,
            'web.Production.config': WebConfigProcessor,
            'web.Staging.config': WebConfigProcessor,
            'app.config': AppConfigProcessor,
            # 'packages.config': PackageConfigProcessor
        }

    def process(self):
        for key, value in self.proj_dict.items():
            if not self.processor_mapping.get(key):
                continue
            processor = self.processor_mapping[key](value)
            # print('PROCESSOR:', processor)
            processor.process()


if __name__ == '__main__':
    explore = SolutionExplore(slnFolder)
    projects = explore.get_projects()

    pp = pprint.PrettyPrinter(indent=4)

    for project in projects:
        pp.pprint(project)
        upgrader = ProjectUpgrader(project)
        upgrader.process()
