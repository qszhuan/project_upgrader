# -*- coding: utf-8 -*-

import re
from lxml import etree


class ConfigProcessor(object):
    def __init__(self, file_path):
        self.file_path = file_path

    def _get_namespace(self, root):
        matches = re.match(r'{.*}', root.tag)
        return matches.group(0) if matches else ''

    def process(self):
        project_ = self.file_path
        tree = etree.parse(project_)
        root = tree.getroot()
        ns = self._get_namespace(root)
        # etree.register_namespace('', ns[1:-1])
        self._update(root, ns)
        tree.write(project_,
                   xml_declaration=True,
                   encoding="utf-8",
                   method="xml",
                   pretty_print=True)

    def _update(self, root, ns):
        pass


class AppConfigProcessor(ConfigProcessor):
    def __init__(self, file_path):
        super().__init__(file_path)

    def _update(self, root, ns):
        startup = root.find('.//{}startup'.format(ns))
        if startup:
            print('!!!!!!!' + startup.tag)
            return

        startup = etree.Element('startup')
        startup.append(etree.Element('supportedRuntime', {'version': 'v4.0', 'sku': '.NETFramework,Version=v4.5.2'}))
        root.append(startup)


class WebConfigProcessor(ConfigProcessor):
    def __init__(self, file_path):
        super().__init__(file_path)

    def _update(self, root, ns):
        compilation = root.find('.//{}compilation'.format(ns))
        compilation = root.find('.//{}compilation'.format(ns))
        if compilation:
            compilation.attrib['targetFramework'] = '4.5.2'


class ProjectFileProcessor(ConfigProcessor):
    def __init__(self, file_path):
        super().__init__(file_path)

    def _update(self, root, ns):
        # target_framework_version = root.find('.//{}TargetFrameworkVersion'.format(ns))
        target_framework_version = root.find('.//TargetFrameworkVersion')
        if target_framework_version:
            target_framework_version.text = 'v4.5.2'


class PackageConfigProcessor(ConfigProcessor):
    pass